
// Create a simple server and the following routes with their corresponding HTTP methods and responses:

const http = require("http");

let port = 4000;


http.createServer((request, response) => {


    // If the url is http://localhost:4000/, send a response Welcome to Booking System

    if(request.url == "/" && request.method == "GET"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to booking system");
    }
    //  If the url is http://localhost:4000/profile, send a response Welcome to your profile!
    else if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to your profile");
    }
    // If the url is http://localhost:4000/courses, send a response Here’s our courses available
    else if(request.url == "/courses" && request.method == "GET"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Here's our courses available");
    }
    // If the url is http://localhost:4000/addCourse, send a response Add a course to our resources
    else if(request.url == "/addCourse" && request.method == "POST"){
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Add a course to our resources");
    }
    //  If the url is http://localhost:4000/updateCourse, send a response Update a course to our resources
    else if(request.url == "/updateCourse" && request.method == "PUT"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Update a course to our resources");
    }
    // If the url is http://localhost:4000/archiveCourse, send a response Archive courses to our resources
    else if(request.url == "/archiveCourse" && request.method == "DELETE"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Archive courses to our resources");
    }


}).listen(port);